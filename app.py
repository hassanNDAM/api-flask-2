# app.py
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'This is my first API with Flask.'

if __name__ == '__main__':
    app.run(debug=True)
